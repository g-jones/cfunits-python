.. currentmodule:: cfunits
.. default-role:: obj

.. highlight:: bash

Installation
============

Install with pip
----------------

`cfunits` is in `PyPI <https://testpypi.python.org/pypi/cfunits>`_::

   $ pip install cfunits

Install from source
-------------------

Downloads are available from `PyPI
<https://testpypi.python.org/pypi/cfunits>`_ or from the `bitbucket
repository <https://bitbucket.org/cfpython/cfunits-python/>`_.
Installation instructions and dependencies are in the `README.md
<https://bitbucket.org/cfpython/cfunits-python/src/master/README.md>`_
file.

Issues
------

Please raise any questions or problems through the `cfunits issue
tracker <https://bitbucket.org/cfpython/cfunits-python/issues>`_.
